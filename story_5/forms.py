from django.db import models
from django import forms
from .models import MataKuliah

class Input_Form(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields = [
            'nama_matkul',
            'dosen_matkul',
            'sks_matkul',
            'desc_matkul',
            'smt_matkul',
            'tahun_matkul',
            'gedung_matkul'
        ]
    error_messages = {
        'required' : 'Please Type'
    }
    input_attrs1 = {
        'type' : 'text',
        'placeholder' : 'Masukkan Nama Matkul'
    }
    input_attrs2 = {
        'type' : 'text',
        'placeholder' : 'Masukkan Nama Dosen'
    }
    input_attrs3 = {
        'type' : 'text',
        'placeholder' : 'Masukkan Ruang Kelas'
    }

    nama_matkul = forms.CharField(label='', required=True, max_length=50, widget=forms.TextInput(attrs=input_attrs1))
    dosen_matkul = forms.CharField(label='', required=True, max_length=50, widget=forms.TextInput(attrs=input_attrs2))
    sks_matkul = forms.IntegerField(label='', required=True, min_value=0, max_value = 10)
    desc_matkul = forms.CharField(label='', required=True, widget = forms.Textarea)
    smt_matkul = forms.ChoiceField(label='', required=True, choices=[('gasal', 'Gasal'), ('genap', 'Genap')])
    tahun_matkul = forms.ChoiceField(label='', required=True, choices=[('2021', '2020 / 2021'), ('1920', '2019 / 2020'), ('1819', '2018 / 2019'), ('1718', '2017 / 2018'), ('1617', '2016 / 2017')])
    gedung_matkul = forms.CharField(label='', required=True, max_length=50,  widget=forms.TextInput(attrs=input_attrs3))
    
    