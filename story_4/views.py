from django.shortcuts import render

# Create your views here.

def profile(request):
    return render(request, 'main/story_4_profile.html', {'nbar' : 'profile'})

def experiences(request):
    return render(request, 'main/story_4_experiences.html', {'nbar' : 'experiences'})