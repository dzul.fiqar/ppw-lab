


 $(document).ready(function () {
     $(".judul").click(function() {
         if($(this).children(".card-header").hasClass("active")){
            $(this).children(".card-header").removeClass("active"); 
            $(this).next(".isi").slideUp();
         }
         else{
            $(".card .judul").children(".card-header").removeClass("active"); 
            $(".card .isi").slideUp(); 

            $(this).children(".card-header").addClass("active"); 
            $(this).next(".isi").slideDown();

         }
     });
     
     $(".down").click(function () {
        $(this).parents(".card").insertAfter($(this).parents(".card").next(".card"));
     });

     $(".up").click(function () {
        $(this).parents(".card").insertBefore($(this).parents(".card").prev(".card"));
     });
 });
