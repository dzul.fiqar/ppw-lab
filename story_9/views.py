from django.shortcuts import render
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages


# Create your views here.
@login_required(login_url='login')
def index(request):
    return render(request, "story_9/story_9.html")
    #isinya login sama nama

def register(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        email = request.POST.get('email')
        password = request.POST.get('password')

        user = User.objects.create_user(username=username, email=email)
        user.set_password(password)
        user.save()
        return HttpResponseRedirect('login')
    else:
        return render(request, "story_9/story_9_register.html")


def login_view(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/story_9')
        else:
            messages.error(request, 'Oops, something bad happened')
            return HttpResponseRedirect('login')

    else:
        return render(request, "story_9/story_9_login.html")

def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/story_9')