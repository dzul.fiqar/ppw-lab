from django.urls import path
from .views import home, savekegiatan, savenama

#url for app
urlpatterns = [
    path('', home, name='home'),
    path('savekegiatan', savekegiatan, name='savekegiatan'),
    path('savenama/<str:pk>/', savenama, name='savenama' ),
]