from django.db import models



# Create your models here.
class MataKuliah(models.Model):
    nama_matkul = models.CharField(max_length=50, unique=True)
    dosen_matkul = models.CharField(max_length=50)
    sks_matkul = models.IntegerField()
    desc_matkul = models.TextField()
    smt_matkul = models.CharField(max_length= 5, choices=[('gasal', 'Gasal'), ('genap', 'Genap')])
    tahun_matkul = models.CharField(max_length= 4, choices=[('2021', '2020 / 2021'), ('1920', '2019 / 2020'), ('1819', '2018 / 2019'), ('1718', '2017 / 2018'), ('1617', '2016 / 2017')])
    gedung_matkul = models.CharField(max_length=50)
    
