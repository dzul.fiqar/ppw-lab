from django.db import models
from django import forms
from .models import Nama, Kegiatan

class Input_Kegiatan(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = [
            'nama_kegiatan',
        ]
    error_messages = {
        'required' : 'Please Type'
    }
    input_attrs1 = {
        'type' : 'text',
        'placeholder' : 'Nama kegiatan'
    }

    nama_kegiatan = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=input_attrs1))

class Input_Nama(forms.ModelForm):
    class Meta:
        model = Nama
        fields = [
            'nama',
        ]
    error_messages = {
        'required' : 'Please Type'
    }
    input_attrs1 = {
        'type' : 'text',
        'placeholder' : 'Nama anda'
    }

    nama = forms.CharField(label='Nama', required=True, max_length=30, widget=forms.TextInput(attrs=input_attrs1))
