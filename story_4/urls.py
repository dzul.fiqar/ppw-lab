from django.urls import path
from .views import profile, experiences

#url for app
urlpatterns = [
    path('', profile, name='profile'),
    path('experiences/', experiences, name='experiences'),
]