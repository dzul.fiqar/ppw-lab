from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import resolve
from .views import index, login_view, register

# Create your tests here.
class Story8Test(TestCase):

    def test_story9_url_notlogin(self):
        response = self.client.get('/story_9/')
        self.assertEqual(response.status_code, 302)
    
    def test_story9_url_login(self):
        response1 = self.client.post('/story_9/register' , data={'username' : 'test', 'email' : 'test@gmail.com', 'password' : 'test1234b'})
        response2 = self.client.post('/story_9/login' , data={'username' : 'test', 'password' : 'test1234b'})
        response = self.client.get('/story_9/')
        self.assertEqual(response.status_code, 200)
    
    def test_story9_login_url_exist(self):
        response = self.client.get('/story_9/login')
        self.assertEqual(response.status_code, 200)
    
    def test_story9_register_url_exist(self):
        response = self.client.get('/story_9/register')
        self.assertEqual(response.status_code, 200)
    
    def test_story9_use_index(self):
        found = resolve('/story_9/')
        self.assertEqual(found.func, index)
    
    def test_story9_login_use_login_view(self):
        found = resolve('/story_9/login')
        self.assertEqual(found.func, login_view)
    
    def test_story9_register_use_register(self):
        found = resolve('/story_9/register')
        self.assertEqual(found.func, register)

    
    def test_register_success(self):
        response = self.client.post('/story_9/register' , data={'username' : 'test', 'email' : 'test@gmail.com', 'password' : 'test1234b'})
        self.assertIsNotNone(User.objects.get(username='test'))

    def test_story9_url_login_fail(self):
        response1 = self.client.post('/story_9/register' , data={'username' : 'test', 'email' : 'test@gmail.com', 'password' : 'test1234b'})
        response2 = self.client.post('/story_9/login' , data={'username' : 'test', 'password' : 'test1234'})
        response = self.client.get('/story_9/')
        self.assertEqual(response.status_code, 302)

    def test_story9_url_logout(self):
        response1 = self.client.post('/story_9/register' , data={'username' : 'test', 'email' : 'test@gmail.com', 'password' : 'test1234b'})
        response2 = self.client.post('/story_9/login' , data={'username' : 'test', 'password' : 'test1234b'})
        response = self.client.get('/story_9/logout')
        self.assertEqual(response.status_code, 302)
        
    



