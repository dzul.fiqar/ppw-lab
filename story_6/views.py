from django.shortcuts import render
from .forms import Input_Nama, Input_Kegiatan
from .models import Kegiatan, Nama
from django.http import HttpResponseRedirect

# Create your views here.
def home(request):
    return render(request, "story_6/story_6.html", {'formkegiatan' : Input_Kegiatan, 'formnama' : Input_Nama, 'daftarkegiatan' : Kegiatan.objects.all(), 'daftarnama' : Nama.objects.all()})

def savekegiatan(request):
    kegiatan = Input_Kegiatan(request.POST or None)
    if kegiatan.is_valid() and request.method == 'POST':
        kegiatan.save()
    
    return HttpResponseRedirect('/story_6')

def savenama(request, pk):
    kegiatan = Kegiatan.objects.get(id=pk)
    nama = Input_Nama(request.POST or None)
    if nama.is_valid() and request.method == 'POST':
        nama = nama.save(False)
        nama.kegiatan = kegiatan
        nama.save()
    
    return HttpResponseRedirect('/story_6')