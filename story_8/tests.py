from django.test import TestCase
from django.urls import resolve
from .views import index
import json

# Create your tests here.
class Story8Test(TestCase):

    def test_story8_url_exist(self):
        response = self.client.get('/story_8/')
        self.assertEqual(response.status_code, 200)
    
    def test_story8_use_index(self):
        found = resolve('/story_8/')
        self.assertEqual(found.func, index)

    def test_template_index(self):
        response = self.client.get('/story_8/')
        html_response = response.content.decode('utf8')
        self.assertTemplateUsed(response, 'story_8/story_8.html')
        self.assertIn("NO", html_response)
        self.assertIn("JUDUL", html_response)
        self.assertIn("PENERBIT", html_response)
        self.assertIn("GAMBAR", html_response)
        self.assertIn("LINK", html_response)
    
    def test_caridata(self):
        response = self.client.get('/story_8/caridata?q=alfa')
        self.assertEqual(response.status_code, 200)
        self.assertLessEqual(len(json.loads(response.content)), 10)




