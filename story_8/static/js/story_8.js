$(document).ready(function () {
    var acak = 'abcdefghijklmnopqrstuvwxyz'
    var kata = acak[Math.floor(Math.random() * acak.length)]

    function insertdata(kata) {
        $.ajax({
            url : "caridata?q=" + kata,
            success : function(data) {
                var daftar = data.items;
                $("#daftarbuku > tbody").empty();
                $("#daftarbuku").append("<tbody>");
                for(i = 0; i < daftar.length; i++) {
                    var nomor = i+1
                    var judul = daftar[i].volumeInfo.title;
                    var publisher = daftar[i].volumeInfo.publisher;
                    var thumbnail = daftar[i].volumeInfo.imageLinks.smallThumbnail;
                    var link = daftar[i].volumeInfo.infoLink;
    
                    $("#daftarbuku").append("<tr><td class=\"h5\">" + nomor + "</td><td class=\"h5\">" 
                        + judul + "</td><td class=\"h5\">" + publisher + "</td><td><img src=" 
                        + thumbnail + "></td><td><a class=\"btn btn-success\" href=\"" + link + "\" role=\"button\" target=\"_blank\">View</a></td></tr>");
                }
    
                $("#daftarbuku").append("</tbody>");
    
            }
        });    
    }
    insertdata(kata);
    
    $("#pencarian").keyup(function (e) {
        var kata = $("#pencarian").val();
        if (kata.length == 0) {
            var acak = 'abcdefghijklmnopqrstuvwxyz'
            var kata = acak[Math.floor(Math.random() * acak.length)]
            insertdata(kata)
        }
        else{
            insertdata(kata)  
        }
    });
 });
 