from django.test import TestCase
from django.urls import resolve
from .views import home, savekegiatan, savenama
from .models import Nama, Kegiatan

# Create your tests here.
class Story6Test(TestCase):
    def setUp(self):
        contoh_kegiatan = Kegiatan.objects.create(nama_kegiatan="Belajar")

    def test_story6_url_exist(self):
        response = self.client.get('/story_6/')
        self.assertEqual(response.status_code, 200)
    
    def test_story6_use_home(self):
        found = resolve('/story_6/')
        self.assertEqual(found.func, home)

    def test_model_nama(self):
        kegiatan = Kegiatan.objects.get(id=1)
        nama = Nama.objects.create(nama='amin', kegiatan=kegiatan)
        self.assertEqual(kegiatan, nama.kegiatan)
        self.assertEqual('Belajar', kegiatan.nama_kegiatan)
        self.assertEqual('Belajar', nama.kegiatan.nama_kegiatan)
    
    def test_template_home(self):
        response = self.client.get('/story_6/')
        self.assertTemplateUsed(response, 'story_6/story_6.html')
    
    def test_story6_use_savekegiatan(self):
        found = resolve('/story_6/savekegiatan')
        self.assertEqual(found.func, savekegiatan)
    
    def test_story6_url_savekegiatan(self):
        response = self.client.get('/story_6/savekegiatan')
        self.assertEqual(response.status_code, 302)

    def test_story6_url_savenama(self):
        response = self.client.get('/story_6/savenama/1/')
        self.assertEqual(response.status_code, 302)
    

    def test_story6_savekegiatan(self):
        response = self.client.post('/story_6/savekegiatan', data={'nama_kegiatan' : 'Membaca'})
        self.assertEqual(Kegiatan.objects.all().count(), 2)
     
        
    def test_story6_savenama(self):
        response = self.client.post('/story_6/savenama/1/', data={'nama' : 'Ahmad'})
        self.assertEqual(Nama.objects.all().count(), 1)
        self.assertEqual(Nama.objects.get(id=1).kegiatan, Kegiatan.objects.get(id=1))
        
