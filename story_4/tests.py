from django.test import TestCase

# Create your tests here.
class Story4Test(TestCase):
    def test_story4_profile_url_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_story4_experiences_url_exist(self):
        response = self.client.get('/experiences/')
        self.assertEqual(response.status_code, 200)