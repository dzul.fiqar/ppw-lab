from django.urls import path
from .views import index, save, detail, lst, delete

#url for app
urlpatterns = [
    path('', index, name='index'),
    path('save', save, name='save'),
    path('list', lst, name='list'),
    path('detail/<str:pk>/', detail, name="detail"),
    path('delete/<str:pk>/', delete, name="delete"),
]