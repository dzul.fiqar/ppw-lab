from django.urls import path
from .views import index, caridata

#url for app
urlpatterns = [
    path('', index, name='index'),
    path('caridata', caridata, name='caridata')
]