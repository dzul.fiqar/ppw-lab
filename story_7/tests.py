from django.test import TestCase
from django.urls import resolve
from .views import index

# Create your tests here.
class Story7Test(TestCase):

    def test_story7_url_exist(self):
        response = self.client.get('/story_7/')
        self.assertEqual(response.status_code, 200)
    
    def test_story7_use_index(self):
        found = resolve('/story_7/')
        self.assertEqual(found.func, index)

    def test_template_index(self):
        response = self.client.get('/story_7/')
        html_response = response.content.decode('utf8')
        self.assertTemplateUsed(response, 'story_7/story_7.html')
        self.assertIn("Profile", html_response)
        self.assertIn("Activities", html_response)
        self.assertIn("Organization / Committee", html_response)
        self.assertIn("Achievement", html_response)
        self.assertIn("Contact", html_response)
        self.assertIn("Dzul Fiqar Aditya Widhiartanto", html_response)



