from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'story_7/story_7.html')