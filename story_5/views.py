from django.shortcuts import render
from .forms import Input_Form
from .models import MataKuliah
from django.http import HttpResponseRedirect

# Create your views here.
def index(request):
    return render(request, 'story_5/story_5.html', {'form' : Input_Form, 'matkuls' : MataKuliah.objects.all()})

def save(request):
    form = Input_Form(request.POST or None)
    if form.is_valid() and request.method == 'POST':
        form.save()
    
    return HttpResponseRedirect('/story_5')

def lst(request):
    return render(request, 'story_5/story_5_list.html', {'matkuls' : MataKuliah.objects.all()})

def detail(request, pk):
    matkul = MataKuliah.objects.get(id=pk)
    return render(request, "story_5/story_5_detail.html", { "matkul" : matkul})

def delete(request, pk):
    matkul = MataKuliah.objects.get(id=pk)
    if request.method == 'POST':
        matkul.delete()
    
    return HttpResponseRedirect('/story_5')