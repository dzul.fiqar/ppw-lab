from django.shortcuts import render
from django.http import JsonResponse
import django.contrib.sites.requests
import json
import urllib3


# Create your views here.
def index(request):
    return render(request, 'story_8/story_8.html')

def caridata(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    
    http = urllib3.PoolManager()

    result = http.request('GET', url)

    return JsonResponse(json.loads(result.data.decode('UTF-8')))