from django.test import TestCase
from .models import MataKuliah
from .views import save

# Create your tests here.
class Story5Test(TestCase):
    def setUp(self):
        mata_kuliah= MataKuliah.objects.create(nama_matkul='SDA', dosen_matkul='Bu Naya', sks_matkul=3, desc_matkul='test', smt_matkul='gasal', tahun_matkul='2021', gedung_matkul='2.2401')

    def test_story5_url_exist(self):
        response = self.client.get('/story_5/')
        self.assertEqual(response.status_code, 200)
    
    def test_story5_save(self):
        response = self.client.post('/story_5/save', data={'nama_matkul' : 'PPW',  'dosen_matkul' : 'Bu Iis', 'sks_matkul' : 3, 'desc_matkul' : 'test', 'smt_matkul' : 'gasal', 'tahun_matkul' : '2021', 'gedung_matkul' : '2.2301'})

        self.assertEqual(MataKuliah.objects.get(id=2).nama_matkul, 'PPW')

    def test_story5_list_url_exist(self):
        response = self.client.get('/story_5/list')
        self.assertEqual(response.status_code, 200)
    
    def test_story5_detail_url_exist(self):
        response = self.client.get('/story_5/detail/1/')
        self.assertEqual(response.status_code, 200)

    def test_story6_savenama(self):
        response = self.client.post('/story_5/delete/1/')
        self.assertEqual(MataKuliah.objects.all().count(), 0)