from django.contrib import admin
from .models import Kegiatan, Nama

# Register your models here.
admin.site.register(Kegiatan)
admin.site.register(Nama)